/*
http://codercareer.blogspot.com/2011/09/no-03-maximum-sum-of-all-sub-arrays.html

Question: A sub-array has one number of some continuous numbers. 
Given an integer array with positive numbers and negative numbers, 
get the maximum sum of all sub-arrays. Time complexity should be O(n).

For example, in the array {1, -2, 3, 10, -4, 7, 2, -5}, 
its sub-array {3, 10, -4, 7, 2} has the maximum sum 18.

solution: figure out, if its negative, dump the value.

*/

let arr = [1, -2, 3, 10, -4, 7, 2, -5];
let gMax = 0;
let localMax = 0;
for (let i = 0; i < arr.length; i++){
    localMax += arr[i];
    if(localMax <= 0)
        localMax = 0;
    if(localMax > gMax)
        gMax = localMax;
}
console.log(gMax);