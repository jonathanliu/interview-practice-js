/*Given a list of integers and a target number, list all pairs that sum up to that number  

arr = [1,6,3,8,2]  //unsorted
target = 4

1. brute force  O(N^2)

2. sort, 
[1, 3, 6, 8]
 ^n       ^x
and then, incr min index and decr max index depending on if too large or too small
if output matches, print, and shift both indexes, until indexes meet. 
quicksort O(nlogn), and then O(n) find matches.

3. make a hash table of complemented numbers seen, and compare at every moment
hash table look up O(1). hash table 
time O(n), space O(n+n)
*/

let arr = [1,6,3,8,2];  //unsorted
target = 5;

function listPairs(array,T){
    let complement = {}; //hash table look up
    for (let i = 0; i < arr.length; i++) {
        if( complement.hasOwnProperty(array[i]) ) {
            console.log(array[i]," ",T-array[i]," sums up to ",T);
        }else {
            complement[T-array[i]] = true;
        }
    }
}
listPairs(arr,target);
