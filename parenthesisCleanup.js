/*  given string with alpha numeric char and parenthesis
return string with balanced parenthesis removing fewest char possible
balance("()") -> "()"
balance("a(b)c)") -> "a(b)c"
balance("((((()") -> "()"
balance("(()()(") -> "()()"
balance(")(())(") -> "(())"
*/
balance("()");
balance(")(");
balance("a(b)c)");
balance("((((()");
balance("(()()(");
balance(")(())(");
balance("))))()(");

function balance(str){
    let result = str;
    let delE = [];

    for(let i=0; i<result.length; i++){
        if(result[i] === "(") {
            delE.push(i);
        }
        else if(result[i] === ")"){
            let last = delE[delE.length-1]; //last index
            if(result[last] == "(") {
                delE.pop();
            }
            else { 
                delE.push(i);
            }
        }
    }
    console.log(delE); //list of indexes to delete.
    while(delE.length){
        ele = delE.pop(); //last of list to pop.
        result = result.slice(0,ele) + result.slice(ele+1,result.length);
    }
    console.log("in", str, "out:" ,result, '\n');
}


// function balance(str){
//     console.log('\ngiven ', str);
//     let result = [];
//     let cLeft = 0;
//     let cRight = 0;

//     for(let i=0; i<str.length; i++){
        
//         if(str[i] === "("){
//             cLeft++;
//             result += str[i];
//         }else if(str[i] === ")" ) {
//             if(cLeft > cRight) {
//                 cRight++;
//                 result += str[i];
//             }
//         }else{
//             //non parenthesis
//             result += str[i];
//         }
        
//     }
//     console.log('halfway',result);
//     let delElement = [];
//     //clean up extra left "("
    
//     for(let i=0; i<result.length; i++){
//         if(result[i] === "("){
//             delElement.push(i);//store location of "left"
//         }else if(result[i] === ")" && delElement.length>0){
//             delElement.pop();
//         }
//     }
    
//     while(delElement.length){
//         ele = delElement.pop();
//         result = result.slice(0,ele) + result.slice(ele+1,result.length);
        
//     }

//     console.log("out:" ,result);
// }