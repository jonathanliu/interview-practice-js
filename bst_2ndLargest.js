/**
 * http://blog.gainlo.co/index.php/2016/06/03/second-largest-element-of-a-binary-search-tree/
 * Second Largest Element of a Binary Search Tree
 * can do in order tree traversal, time O(n)
 * or below which is, time O(logn)
 */


function secondLargest (node){
    let num2;

    if(node.left == null && node.right == null)
        return false;

    while(node.right){
        num2 = node.data;
        node = node.right;            
    }
    if(node.left != null){
        node = node.left;
        while(node.right)
            node = node.right;
        num2 = node.data;
    }
    return num2;
}
secondLargest(head);