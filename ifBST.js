//Write an algorithm to verify if a tree is a binary search tree  



function isBST(node) {
    let result = true;

    function traverse(node, min, max) {
        if(node == null)
            return;
        if(node.data < min || node.data > max)
            return result = false;

        if(node.left) 
            traverse(node.left, min, node.data);            
        if(node.right) 
            traverse(node.right, node.data, max);            
    }
    traverse(node, -Infinity, Infinity);
    return result;
}