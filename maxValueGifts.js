/*
No. 56 - Maximal Value of Gifts
Question: A board has n*m cells, and there is a gift with some value 
(value is greater than 0) in every cell. You can get gifts starting 
from the top-left cell, and move right or down in each step, and finally
 reach the cell at the bottom-right cell. What’s the maximal value of 
 gifts you can get from the board?

*/

let maxGifts = function(grid){
    let maxV = [];
    for(let i = 0; i< grid.length; i++)
        maxV[i] = [];

    maxV[0][0] = grid[0][0]; //start top left pt

    for(let i = 1; i < grid.length; i++)  //max values of 1st row and 1st col.
        maxV[i][0] = maxV[i-1][0] + grid[i][0];
    for(let j = 1; j < grid[0].length; j++)
        maxV[0][j] = maxV[0][j-1] + grid[0][j];
    console.log('b',maxV);

    //as i fill out the rest of the grid,
    for(let i = 1; i < grid.length; i++){
        for(let j = 1; j < grid[0].length; j++){
            maxV[i][j] = Math.max( maxV[i-1][j], maxV[i][j-1] ) + grid[i][j];
        }
    }
    console.log('c',maxV);
}

let grid = [
    [2,5,6,1],
    [10,4,3,6],
    [5,6,3,3],
    [1,7,6,4]];
maxGifts(grid);