

function isPalindrome(str) {    
    console.log(str.split(''));
    console.log(str.split('').reverse());
    console.log(str.split('').reverse().join(''));
    return (str === str.split('').reverse().join(''));    
}

function isPalindromeRegex(val) {    
    var str = val.toString().toLowerCase().replace(/[^a-z]/g,"");
    return (str === str.split('').reverse().join(''));    
}


console.log( isPalindromeRegex('A dog, a plan, a canal: pagoda.') );
console.log( isPalindrome('radecar') );