//http://blog.gainlo.co/index.php/2016/06/12/flatten-a-linked-list/


function Tree (){
    let head = null;
    
    let Node = function(data){
        this.data = data;
        this.left = null;
        this.right = null;
    }

    this.add = ((data)=>{
        //if(head==null){
        head = new Node(data);            
                
        head.left = new Node(2);
        head.left.left = new Node(3);
        head.left.left.left = new Node(4);
        head.left.left.left.left = new Node(7);
        head.left.left.left.right = new Node(9);

        head.left.right = new Node(5);       
        head.left.right.left = new Node(6);
        head.left.right.right = new Node(8);
    });

    this.printBFS = function(){
        console.log('start', head);
        
        if(head==null)
            return;
        let queue = [];
        queue.push(head);
        let myList = [];        
        while(queue.length > 0){ 
            let node = queue.pop();
            myList.push(node.data);            
            if(node.left)
                queue.push(node.left); 
            if(node.right)
                queue.push(node.right);
        }
        
        console.log(myList);
    }
}

let myTree = new Tree();
myTree.add(1);
myTree.printBFS();



    


