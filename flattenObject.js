//fibonacci with memoiz

/*
print parenthesis
given n
if n = 3,
then output all variations of
()()()
((()))
...

number of ( = left
num of ) = right
left >= right
ends when we get the 3rd right or outputs n*2.
*/


// var keys = {'key1':1,'key2':{'key3':2,'key4':3,'key5':{'key6':4}}}, result = {};
// console.log(keys);
// function serialize(keys, parentKey = ""){
//     for(var key in keys){
//         if(typeof keys[key] === 'object'){ //if its a number 
//             serialize(keys[key], parentKey+key+".");
//         }else{                       //else its a child obj            
//             result[parentKey+key] = keys[key]; 
//         }
//     }
// }
// serialize(keys);
// console.log(result);

var keys = {'key1':1,'key2':{'key3':2,'key4':3,'key5':{'key6':4}}}


let flat = {};
function flatten(keys, parentkey = ''){
    for(key in keys){
        if(typeof keys[key] === 'object'){
            flatten(keys[key], parentkey + key + '.')
        }else{
            flat[parentkey + key] = keys[key];
        }
    }
}
flatten(keys);
console.log(flat);