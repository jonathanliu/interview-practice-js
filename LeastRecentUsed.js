/*
https://chrisrng.svbtle.com/lru-cache-in-javascript
(least recently used) cache implementation
*/

let Node = function(key, value){
    this.key = key;
    this.value = value;
    this.prev = null;
    this.next = null;
}

function lru(limit) {
    this.size = 0;
    this.limit = limit; //cache size
    this.map = {};
    this.head = null;
    this.tail = null;

    this.set = function (key, value) {
        let node = new Node(key, value);
        if(key in this.map){ //if exists
            this.map[key].value = node.value; //update hash value
            this.remove(node.key); //remove from doubly linked list
        }
        else { //doesnt exist
            if (this.size >= this.limit) {
                delete this.map[this.tail.key];
                this.size--;
                this.tail = this. tail.prev;
                this.fail.next = null;
            }
        }
        this.setHead(node); //add to front of list
    }

    this.get = function(key) {
        if (this.map[key]){
            let value = this.map[key].value;
            this.remove(key);
            this.set(key,value);
            return value;
        }
        return "does not exist";
    }

    this.remove = function(key) {
        let node = this.map[key];
        
        //remove doubly linked list node
        if(node.prev != null)
            node.prev.next = node.next;
        else
            this.head = node.next;
        if(node.next != null)
            node.next.prev = node.prev;
        else
            this.tail = node.prev;
        
        delete this.map[key];
        this.size--;
    }
}