/*

given an array and a window of W, whats the max sum?

ie. arr = [2 1 3 5 0 1 4], W = 3    then output: 9


solution: add all. then minus 1 and 2, see if thats the max. add 1, minus 3, see if thats the max....
*/

let arr = [5, 6, 7, 1, 2, 3, 4];
slidingWindow(arr,3);
function slidingWindow(arr, winSize){
    if(winSize>arr.length)
        winSize=arr.length;
    
    let sum=0, maxSum=0;

    for(let i=0; i < arr.length; i++){
        sum=0;
        for(let j=0; j < winSize; j++){
            //console.log(i,j, arr[ (i+j)% arr.length] )
            sum += arr[ (i+j)% arr.length] ;
        }
        
        if(sum > maxSum)
            maxSum = sum;
    }
    console.log( maxSum );
}

//solution/faster
slideWin(arr,3);
function slideWin(arr, winSize){
    if(winSize>arr.length)
        winSize=arr.length;
    
    let sum=0, maxSum=0;

    for(let i=0; i < winSize; i++){
        sum += arr[i];
    }
    maxSum = sum;
    for(let i=0; i < arr.length; i++){
        sum = sum - arr[i] + arr[ (winSize+i) % arr.length]; //modulo to wrap around.       
        if(sum > maxSum)
            maxSum = sum;
    }
     
    console.log( maxSum );
}