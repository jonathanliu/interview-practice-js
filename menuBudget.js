/*Given a restaurant menu and a budget, 
output all the  possible ways to use up the budget.
*/
const p = [7, 3, 2.4, 5, 2.9, 1];
budget = 6;

h(budget,[], 0);
//TODO add memozation
function h(cash, dishNum, i){
    //console.log(cash,dishNum,i);
    if(cash<=0 || i >= p.length){
        console.log('final',dishNum);
        return;
    }
    if(p[i]<=cash){
        //buy the dish
        dishNum.push(i); 
        h(cash-p[i], dishNum, i+1);

         //dont buy the dish
        dishNum.pop();
        h(cash, dishNum, i+1);
    }else{
         //dont buy the dish
        h(cash, dishNum, i+1);
    } 
}
