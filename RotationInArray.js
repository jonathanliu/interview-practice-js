/*
http://codercareer.blogspot.com/2013/03/no-47-search-in-rotation-of-array_31.html
Search in a Rotated Array

find "K" in a rotated "ARRAY".

*/
let array = [3, 4, 5, 1, 2];

function find(array, k, start, end) {
    console.log(start, end);
    if (start > end)
        return -1;

    let middle = Math.floor(start + (end - start) / 2);
    if (array[middle] == k)
        return middle;

    if (array[middle] >= array[start]) { //1st half ascending
        // the middle number is in the first increasing sub-array
        if (k >= array[start] && k < array[middle])  //if k 1st half
            return find(array, k, start, middle - 1); //look at left
        return find(array, k, middle + 1, end); //look at right
    }
    else {//} if(array[middle] <= array[end]) {        
        // the middle number is in the second increasing sub-array
        if (k > array[middle] && k <= array[end]) //if k 2nd half ascd
            return find(array, k, middle + 1, end);
        return find(array, k, start, middle - 1);
    }
    // It should never reach here if the input is valid
    return false;
}
console.log(find([3, 4, 5, 6, 7, 8, 9, 10, 1, 2], 4, 0, 10));
console.log(find([3, 4, 5, 1, 2], 2, 0, array.length - 1));
//console.log(find([3,4,5,1,2],0,array.length-1));
//console.log(find([2,3,4,5,1],0,array.length-1));