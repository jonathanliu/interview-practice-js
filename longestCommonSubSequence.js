/**
 * https://www.geeksforgeeks.org/longest-common-subsequence/
 LCS for input Sequences “ABCDGH” and “AEDFHR” is “ADH” of length 3.
LCS for input Sequences “AGGTAB” and “GXTXAYB” is “GTAB” of length 4.
 
solution
think from the end.
case 1: equals
    str1: _____A    str2: _____A
    then return 1 + LCS(str1(n-1), str2(n-1))
case 2: doesnt equal
    str1: _____A    str2: _____B
    then return max(LCS(str1(n-1), str2(n)),  LCS(str1(n), str2(n-1))))
*/

function LCS(str1, str2) {
  let n1 = str1.length - 1;
  let n2 = str2.length - 1;

  let mem = [];
  for (let k = 0; k < str1.length; k++) mem.push([]);

  let phrase = [];
  console.log(trav(n1, n2));

  phrase = phrase.join("");
  console.log(phrase);

  function trav(i, j) {
    if (i < 0 || j < 0) return 0;
    if (mem[i][j] != undefined) return mem[i][j];
    let result;
    if (str1[i] === str2[j]) {
      phrase[i] = str1[i];
      result = 1 + trav(i - 1, j - 1);
    } else {
      result = Math.max(trav(i - 1, j), trav(i, j - 1));
    }
    mem[i][j] = result;
    return result;
  }
}
LCS("BAGETD", "ABACD");
