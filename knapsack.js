//knapsack
//given
let value = [60, 100, 120];
let weight = [10, 20, 30];
let maxW = 50;

/**find max value
 *
 * think of DP + memo
 *
 * start at last item,
 * case 1: we add the value
 * case 2: we dont add the value
 * and decrement
 * */

 
// let mem = [];//todo
// for(i=0; i<maxW;i++)
//     mem.push([]);

// function ks(i, sumW){
//     if(i<0||sumW<=0)
//         return 0;
//     if(mem[i][sumW] != undefined)
//         return [i][sumW];
//     let result;

//     if(weight[i] <= sumW){
//         let tmp1 = value[i] + ks(i-1, sumW-weight[i] );
//         let tmp2 = ks(i-1, sumW);
//         result = Math.max( tmp1, tmp2);
//     }else
//         result = ks(i-1, sumW);

//     mem[i][sumW] = result;
//     return result;
// }
// console.log(  ks(value.length, maxW));
