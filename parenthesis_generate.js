//https://github.com/chihungyu1116/leetcode-javascript/blob/master/22%20Generate%20Parentheses.js

/**
 * @param {number} n
 * @return {string[]}
 */

var generateParenthesis = function(n) {
  var res = [];
  bfs("", 0, 0);
  return res;

  function bfs(str, countL, countR) {
    if (countL === n && countR === n) {
      return res.push(str);
    }
    if (countL < n) {
      bfs(str + "(", countL + 1, countR);
    }
    if (countL > countR) {
      bfs(str + ")", countL, countR + 1);
    }
  }
};

// console.log(generateParenthesis(3));
