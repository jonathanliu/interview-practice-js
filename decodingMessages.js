/*
https://practice.geeksforgeeks.org/problems/total-decoding-messages/0
Given encoded message "123",  it could be decoded as "ABC" (1 2 3) or "LC" (12 3) or "AW"(1 23).
So total ways are 3.
*/
const hashletter = {
    'A': '1', 'B': '2', 'C': '3', 'D': '4', 'E': '5',
    'F': '6', 'G': '7', 'H': '8', 'I': '9', 'J': '10',
    'K': '11', 'L': '12', 'M': '13', 'N': '14', 'O': '15',
    'P': '16', 'Q': '17', 'R': '18', 'S': '19', 'T': '20',
    'U': '21', 'V': '22', 'W': '23', 'X': '24', 'Y': '25', 'Z': '26'
}
function stringtoNum(str){
    let sNum = ''
    for(let i=0;i<str.length;i++){
        sNum += hashletter[ str[i] ];
    }
    return sNum;
}
function numWaysStr(str){
    let sNum = stringtoNum(str);
    if(sNum[0] === '0' || sNum === '')
        return null;
    
    let count = numWaysStr(str.slice(1,str.length))

    if(sNum.length >= 2 && Number(str.slice(0,1)) < 27){
        count = count + numWaysStr(str.slice(2,str.length))        
    }
    return count;
}
//console.log( numWaysStr('AZB') );
//console.log( numWaysStr('ABC') );

function numWaysNum(sNum){
    if(sNum.length == 0)
        return 1;    
    if(sNum[0] == '0')
        return 0;
    let count = numWaysNum( sNum.slice(1,sNum.length) )

    if(sNum.length >= 2 && Number(sNum.slice(0,2)) < 27){
        count = count + numWaysNum( sNum.slice(2,sNum.length))
    }
    return count;
}
console.log('me',  numWaysNum('123') );
console.log('me',  numWaysNum('5123') );

//*********************************************************************************************** */
//Solution 2,  from numbers to strings
function csDojo(data, k){
    if(k ==0)
        return 1;
    let s = data.length - k;
    if (data[s] == '0')
        return 0;

    let result = csDojo(data, k-1)
    if ( k >=2 && Number(data.slice(s,s+2)) <= 26)
        result += csDojo(data, k-2)
    return result;
}
let data = '123';
console.log( csDojo(data,data.length) );
data = '5123';
console.log( csDojo(data,data.length) );