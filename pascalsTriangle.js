/** n = num of rows */
function pascal(n) {
  let res = [];
  if (n >= 1)
    res.push([1]);
  if (n >= 2) {
    //rowI is the row index
    //e is the element in row
    for (let rowI = 1; rowI <= n; rowI++) {
      let line = [1];
      for (let e = 1; e <= rowI; e++) {
        if (e == rowI)
          line.push(1);
        else
          line.push(res[rowI - 1][e] + res[rowI - 1][e - 1]);
      }
      res.push(line);
    }
  }
  return res;
}
console.log(pascal(5));
