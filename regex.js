



function myfunc(str){
    let filter = /AB.*E/;
    return filter.exec(str);
}

console.log(myfunc('FABCDE'));


let str = 'race car';       //ignore white space
str = str.replace(/\s/g, '');
console.log(str);