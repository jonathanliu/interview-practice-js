/*
http://blog.gainlo.co/index.php/2016/07/06/lowest-common-ancestor/
https://www.youtube.com/watch?v=TIoCCStdiFo
*/


/**
 * @param {*} Node node 
 * @param {*} Node A 
 * @param {*} Node B 
 * 
 * Node (data){
 *  this.data = data;  A number or string
 *  this.left = Node;  A node or null
 *  this.right = Node; A node or null
 * }
 */
function LCA (node, A, B){
    if(node == null)
        return null;
    if(node.data == A.data || node.data == B.data)
        return node.data;

    let left = LCA(node.left, A, B);
    let right = LCA(node.right, A, B);

    if (left == null && right == null)
        return null;
    if (left != null && right != null)
        return node.data;
    return (left == null)? right : left;
}
