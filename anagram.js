/*
http://blog.gainlo.co/index.php/2016/05/06/group-anagrams/

group anagrams together

get anagram of word, then put into hash.
*/
let arr = ['cat', 'dog', 'act', "door", "odor"];

let result = {};
for(let i=0; i<arr.length; i++) {
    let sorted = sortWord(arr[i]);
    let currA = result[sorted] || [];
    currA.push(arr[i]);     
    result[sorted] = currA;
    console.log(result);
}
for (keys in result){
    console.log(result[keys]);
}

function sortWord(word) {
    return word.split('').sort().join('');
}