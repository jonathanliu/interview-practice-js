/*  Determine if any 3 integers in an array sum to 0.

Note: The following solutions assumes that repetitions 
(i.e. choosing the same array element more than once) 
are *allowed*, so the array [-5,1,10] contains a zero 
sum (-5-5+10) and so does [0] (0+0+0).

[4, 2, -1, 1, -5, 6, -4] = True  

1. brute force solution  O(n^3)

2. sort?
b/c 3 integers, wouldnt repeat num more than 2x, unless its zero.
create a new array, sums all pairs, and then see if it equals the negative old array. O(n^2 + n)
time O(n^2)
*/

let array = [4, 2, -1, 1, 6, -4];

function threeSumZero(arr){
    arr.sort((a,b)=>{return a-b});  //O(nlogn) quicksort
    //as i sort, if i find any zero, return true;

    console.log(arr);
    for(let num3 = 0; num3 < arr.length; num3++){
        //2 sum prob
        let num1 = 0;
        let num2 = arr.length-1;
        
        while(num1 <= num2){
            //console.log(arr, num1, num2, num3);
            if(arr[num1] + arr[num2] === -arr[num3])
                return true;
            if(arr[num1] + arr[num2] > -arr[num3])
                num2--;
            else //if (arr[num1] + arr[num2] < -arr[num3])
               num1++;
        }
    }
    return false;
}
console.log(threeSumZero(array));

