/*Find running median from a stream of integers 

Step 1: Add next item to one of the heaps

   if next item is smaller than maxHeap root add it to maxHeap,
   else add it to minHeap

Step 2: Balance the heaps (after this step heaps will be either balanced or
   one of them will contain 1 more item)

   if number of elements in one of the heaps is greater than the other by
   more than 1, remove the root element from the one containing more elements and
   add to the other one
Then at any given time you can calculate median like this:

   If the heaps contain equal amount of elements;
     median = (root of maxHeap + root of minHeap)/2
   Else
     median = root of the heap with more elements

*/

var heap = require('heap');

var minHeap = [];
var maxHeap = [];
var total = 0;

function insert(num) {
  if (total % 2 === 0) {
    // If this is an even element add it to maxHeap
    heap.push(maxHeap, -1 * num);
    total++;

    if (minHeap.length === 0) {
      return;
    }

    // If maxHeap's root became greater than minHeap's root then swap the roots
    if (-1 * maxHeap[0] > minHeap[0]) {
      toMin = -1 * heap.pop(maxHeap);
      toMax = -1 * heap.pop(minHeap);
      heap.push(maxHeap, toMax);
      heap.push(minHeap, toMin);
    }
  } else {
    // If this is an even element add it to max head. Then pop the lowest element
    // and add it to the min heap
    var toMin = -1 * heap.pushpop(maxHeap, -1 * num);
    heap.push(minHeap, toMin);
    total++;
  }
}

function getMedian() {
  if (total % 2 === 0) {
    // If the number of elements is even then we need to get both roots and
    // divide them by two
    return (-1 * maxHeap[0] + minHeap[0]) / 2;
  } else {
    // If the number of elements is odd return the head of the max heap
    return -1 * maxHeap[0];
  }
}