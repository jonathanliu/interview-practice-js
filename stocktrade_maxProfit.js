/*
You are to write pseudo code O(n) algorithm to maximize a
one day trade. You will have 5 days of predicted prices 
and your algorithm must choose what day to buy and sell
to maximize gains.

https://www.glassdoor.com/Interview/You-are-to-write-pseudo-code-O-n-algorithm-to-maximize-a-one-day-trade-You-will-have-5-days-of-predicted-prices-and-your-QTN_1963588.htm
https://stackoverflow.com/questions/7086464/maximum-single-sell-profit/33315136#33315136
*/

let price = [5,10,4,6,12];

function profit(array){
    let min = 0;
    let maxPrice = -1;


    for(let i = 0; i < array.length; i++){
        if(array[i] < array[min])
            min = i;
        else if(array[i] - array[min] > maxPrice)
            maxPrice = array[i] - array[min];            
    }

    console.log(maxPrice);
}
profit(price);

//     let min = 0;
//     let maxPrice=0;
//     let Tmin =0, Tmax=0; //overall total

//     for(let i = 0; i < array.length; i++){
//         if(array[i] < array[min]){
//             min = i; //local min.
//         }
//         if(array[i]-array[min] > maxPrice){
//             Tmin = min;
//             Tmax = i;
//             maxPrice = array[i]-array[min];
//         }
//     }
//     console.log('buy at ', array[Tmin], 'and sell at', array[Tmax],'for a profit of $', maxPrice);
