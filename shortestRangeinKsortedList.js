/**
 * given lists
 * [4,10,15,24]
 * [0,9,12,20]
 * [5,18,22,30]
 * 
 *  best time O(nk)
 * 
 * pick the set of nums(1 from each list)
 * to get the shortest range
 * 
 * ans: 3 ptrs all =0
 * set minrange
 * increment the ptr with min value.
 * check if its a new minrange
 */