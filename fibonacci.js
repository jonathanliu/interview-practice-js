//solution to return an array
function fibonacci_array(num) {
  let arr = [0, 1];
  let index = 2;
  if (num > 2) {
    while (index < num) {
      arr.push(arr[index - 1] + arr[index - 2]);
      index++;
    }
  }
  return arr;
}

console.log(fibonacci_array(7));

//solution to return value or array in mem.
//add memo
let mem = [];
function fib_val(index) {
  if (mem[index]) return mem[index];
  if (index < 2) return index;
  mem[index] = fib_val(index - 1) + fib_val(index - 2);
  return mem[index];
}
console.log(fib_val(6));
