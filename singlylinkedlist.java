

void printrecursive(node *head){
    if(head == null){
        return;
    }
    else {
        printrecursive(head->next);
        cout << head->data << " ";
    }
}


void printIterative(node *head){
    int stack[];

    while(head!=null){
        stack.push(head->data);
        head = head->next;
    }
    while(!stack.isempty()) {
        cout << stack.pop() << " ";
    }
}