/*
Given two strings, return true if they are one edit away from each other,else return false.
An edit is insert/replace/delete a character. 
Ex. {"abc","ab"}->true, 
{"abc","adc"}->true, 
{"abc","cab"}->false
*/
function isOneEdit(str1, str2) {
    if (str1 === str2)
        return false;
    let len1 = str1.length;
    let len2 = str2.length;
    // If the length difference of the stings is more than 1, return false.
    if ( Math.abs(len1-len2) > 1 )
        return false;

    let i = 0, j = 0, diff = 0;    
    while (i<len1 && j<len2) { //incrementally check every char         
        if (str1[i] != str2[j]) { //if theres a diff
            //console.log(str1[i] , str2[j], diff );
            diff++;
            if (diff > 1)
                return false;              
            if (len1 > len2)      //str1 has 1 extra char
                i++;
            else if (len2 > len1) //str2 has 1 extra char
                j++;
            else if (len1 == len2){ //replace 1 char
                i++; 
                j++;
            }
        } else { //if no diff
            i++;
            j++;
        }          
    }
    return true;
}
// console.log( isOneEdit('abc','abde')); //false
// console.log( isOneEdit('abxt','abcd')); //false
// console.log( isOneEdit('abcdef','abcd')); //false
// console.log( isOneEdit('abc','abdc')); //true
// console.log( isOneEdit('abc','ab'));  //true
// console.log( isOneEdit('abc','abcd')); //true
// console.log( isOneEdit('abc','cab')); //true

//Another same O(n) solution
function one_edit_apart(str1, str2) {
    if (str1.length > str2.length){
        let tmp = str1.slice();
        str1 = str2.slice();
        str2 = tmp.slice();
    }
    if (str2.length - str1.length > 1)
      return false;
    let diff = false;
    for (let i = 0, j = 0; i < str1.length; ++i, ++j) {
      if (str1[i] != str2[j]) {
        if (diff) 
            return false;
        diff = true;
        if (str2.length > str1.length) {
          --i;
        }
      }
    }
    return diff || (str2.length != str1.length);
  }
console.log( one_edit_apart('abc','abde')); //false
console.log( one_edit_apart('abxt','abcd')); //false
console.log( one_edit_apart('abcdef','abcd')); //false
console.log( one_edit_apart('abc','abdc')); //true
console.log( one_edit_apart('abc','ab'));  //true
console.log( one_edit_apart('abc','abcd')); //true
console.log( one_edit_apart('abc','cab')); //false