/*
No. 13 - First Character Appearing Only Once
Problem: Implement a function to find the first character in a string which only appears once.
For example: It returns ‘b’ when the input is “abaccdeff”.

ans:
brute force. for every element, check if it repeats, if not, then incr.
Time O(n^2),  space O(1)

use a hashtable, key is letter, value is occurance
go through once, record to hash.
go thru 2nd time, find 1st letter with 1.
Time O(n), space O(n)
*/

let str = "abaccdeff";
let hash = {};
for (let i = 0; i < str.length; i++){
    if(hash.hasOwnProperty(str[i]))
        hash[str[i]] = hash[str[i]] + 1;
    else
        hash[str[i]] = 1;
}
console.log(hash);
for (let i = 0; i < str.length; i++){
    if(hash[str[i]] === 1){
        console.log(str[i]);
        break;
    }
}
