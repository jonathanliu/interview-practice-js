/**Implement a queue ↴ with 2 stacks. ↴ Your queue should 
 * have an enqueue and a dequeue method and it should be 
 * "first in first out" (FIFO).
 * 
 * say stack1 = bottom 1,2,3,4,5  top
 * then stack2 = 5,4,3,2,1
 * then queue can take from top of stack2.
*/

//let myqueue = [];  //can only push and shift.
let stack1 = []; //can only push and pop

let queue = ((e) => {
    stack1.push(e);
    return e;
});

let dequeue = (() => {
    let stack2 = [];
    while (stack1.length){
        stack2.push(stack1.pop());
    }
    let temp = stack2.pop();
    while (stack2.length){
        stack1.push(stack2.pop());
    }
    return temp;
});

console.log( queue(1) );
console.log( queue(2) );
console.log( queue(3) );
console.log( queue(4) );
console.log( dequeue() );
console.log( dequeue() );