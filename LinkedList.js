/**
 * iterate over a singly linked list backwards.
 * a->b->c should print as C B A
 * 
 * there are 4 solutions. state each one and time complexity
 * 
 * solution 1**************************
 * recursive, call child until null, then print data
 *  time O(n), space O(1)
 * * solution 2**************************
 * iterative O(n) mem
 * create an array, as you call out child, store in array,
 * time O(n), space O(n)
 * * solution 3**************************
 * iterative with space O(1) and time O(n^2)
 * for loop, with decrement index, to print once index reached.
 * * solution 4**************************
 * iterative with mem O(1) and time O(n)
 *   change direction of linked list.
 *   A->B  becomes A<-B
 */

function linkedList(){

    let head = null;
    let length = 0;

    let Node = function(data){
        this.data = data;
        this.next = null;
    };

    this.add = function(data){
        let node = new Node(data);

        if (head == null)
            head = node;
        else{
            let currentNode = head;
            while(currentNode.next)
                currentNode = currentNode.next;            
            currentNode.next = node;
        }
        length++;
    };
    
    this.print = (()=>{
        let node = head;
        let str = '';
        while (node.next){
            str += node.data;
            node = node.next;
        }
        str += node.data;
        console.log (str);
    });

    this.printBackwards = (()=>{

        function backRecursive(node){
            if(!node.next){
                return console.log(node.data);
            }else {
                backRecursive(node.next);
                console.log(node.data);
            }
        }
        //backRecursive(head);

        function iterative(node){
            let mem = []; //stack
            while(node.next){
                mem.push(node.data);
                node = node.next;
            }
            mem.push(node.data);

            while(mem.length)
                console.log(mem.pop());
        }
        //iterative(head);

        function iter_n2run(){
            //print the last element, then start over and count one less to print the 2nd to last element...
            //get length
            let node = head;
            let len = 0;
            while(node.next){
                len++;
                node = node.next;
            }

            for(let i = len; i >= 0; i--){
                let tmp = head;
                let index = 0;

                while(index < i){
                    tmp = tmp.next;
                    index++;
                }
                console.log(tmp.data);
            }
        }
        iter_n2run();

        //solution 4
        function iter_nrun(){
            //turn the list from a->b->c  to a<-b<-c, rearranging the next direction.
            let prevNode = new Node(null);
            let currNode = head;
            let nextNode = new Node(null);

            while(currNode.next != null){
                console.log(prevNode, '\n', currNode, '\n', nextNode);
                nextNode = currNode.next; //tmp store
                currNode.next = prevNode; // define the next ptr

                prevNode = currNode; //shift to next set
                currNode = nextNode;
                
            }
            //console.log('1',prevNode, currNode.data, currNode.data)
            currNode.next = prevNode;
            //console.log('2',prevNode.data, currNode.data, currNode.data)
            while(currNode != null){
                console.log(currNode.data+" -> ");
                currNode = currNode.next;
            }
        }
        //iter_nrun();
    });
    
}

let myList = new linkedList();
myList.add('A');
myList.add('B');
myList.add('C');
myList.add('D');
myList.add('E');
//myList.print();
myList.printBackwards();


