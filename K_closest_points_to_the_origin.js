/*
https://www.youtube.com/watch?v=eaYX0Ee0Kcg
question:
let arr = [[-2,4], [0,-2],[-1,0],[3,5],[-2,-3],[3,2]];
print K closest points to origin

answer:
1. make an an array of objects   [{},{},...]  and quicksort
time: O(nlogn)

2. better solution, similar to "[3,5,1,4]" find k smallest points.
b/c max heap = most efficient way to track of largest value,
we have a heap to track largest kth value as we go through the list. so in the end
//pts with distance  O(n)
    //create max heap of distance, with k elements.  O(n-k)
    //replace in max heap O(logk)
    //print out those elements O(k)
    //O(n + (n-k)logk
*/


let arr = [[-2,4], [0,-2],[-1,0],[3,5],[-2,-3],[3,2]];
k = 3;

// Time:  O(nlogn + nlogw), n = len(nums), w = max(nums) - min(nums)
// Space: O(1)

// Binary search with sliding window solution
function smallestDistancePair(nums, k) {
            sort(nums.begin(), nums.end());
            int left = 0, right = nums.back() - nums.front() + 1;
            while (left < right) {
                const auto mid = left + (right - left) / 2;
                if (possible(mid, nums, k)) {
                    right = mid;
                } else {
                    left = mid + 1;
                }
            }
            return left;
        }
    
    private:
        bool possible(const int guess, const vector<int>& nums, const int k) {
            int count = 0, left = 0;
            for (int right = 0; right < nums.size(); ++right) {
                while ((nums[right] - nums[left]) > guess) {
                    ++left;
                }
                count += right - left;
            }
            return count >= k;
        }
    };