
const g = [
            [1,2,3,4],
            [5,6,7,8],
            [9,10,11,12],
            [13,14,15,16]];
//print outside in,
function spiralize(g){
    let left = 0;
    let right = g[0].length-1;
    let top = 0;
    let down = g.length-1;

    //initial state TODO
    let out=[];
    while(true){
        //goright
        if(top>down || left>down)
            break;

        for(let i = left; i <= right;i++){
            out.push(g[top][i]);
        }
        //godown
        top++;
        for(let i = top; i <= down; i++){
            out.push(g[i][right]);
        }
        //go left
        right--;
        for(let i = right; i >= left; i--){
            out.push(g[down][i]);
        }
        //go up 
        down--;       
        for(let i = down; i >= top; i--){
            out.push(g[i][left]);
        }
        left++;       
    }

    console.log(out);
}
spiralize(g);