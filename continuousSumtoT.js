/*
Problem: Given a sequence of nonnegative integers A and an integer T, 
return whether there is a *continuous sequence* of A that sums up to exactly T

Example:
[23, 5, 4, 7, 2, 11], 20. Return True because 7 + 2 + 11 = 20
[1, 3, 5, 23, 2], 8. Return True because 3 + 5 = 8
[1, 3, 5, 23, 2], 7 Return False because no sequence in this array adds up to 7

since non neg and cont, have a start and end index.
if the sum numbers < T, increment end,   which adds sum
if the sum numbers > T, increment start, which reduces sum
time O(n)
*/


let arr = [23, 5, 4, 7, 2, 11];
let T = 20;

function existsContSumT (A, T) {
    let start = 0;
    let end = 0;
    let sum = A[0];
    while(end < A.length){
        if(sum == T)
            return true;
        if(sum < T) {
            end++;
            sum += arr[end];
        }
        if(sum > T) { //sum > T            
            sum -= arr[start];
            start++;
        }
    }
    return false;
}
console.log(existsContSumT(arr, T));