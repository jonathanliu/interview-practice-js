/*Given an array of integers, write an in-place function to bring all the non-zero elements to the left of the array keeping the original order.  
https://www.glassdoor.com/Interview/Given-an-array-of-integers-write-an-in-place-function-to-bring-all-the-non-zero-elements-to-the-left-of-the-array-keeping-QTN_1314648.htm
*/
//let arr = [3,0,0,2,0,0,0,4];
// let countZeros = 0;
// for(let i=0; i<arr.length; i++) {
//    while (arr[i] == 0) {        //find the 1st zero
//         arr.splice(i,1);    //remove zero   splice(start at element, number of elements to delete,...)
//         countZeros++;
//    }
// }
// while(countZeros){
//     arr.push(0);
//     countZeros--;
// }
// console.log(arr);
// arr = 3,4,1,-1,5.... 0]




let arr = [3,0,0,2,0,0,0,4];
let left = 0;           //go to 1st zero from left
let right = arr.length-1; //go to 1st non-zero from right
                        //continue until met up.

while(left < right){
    while(arr[left] > 0 && left < right)
        left++;
    while(arr[right] == 0 && left < right)
        right--;
    
    if(left < right){
        let tmp = arr[left];
        arr[left] = arr[right]
        arr[right] = tmp;
    }
}
console.log(arr);