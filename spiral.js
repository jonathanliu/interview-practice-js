/**
 * Find the pattern and complete the function: 
int[][] spiral(int n);
where n is the size of the 2D array.
Sample Result
input = 3
123
894
765

input = 4
01 02 03 04
12 13 14 05
11 16 15 06
10 09 08 07

input = 8
1 2 3 4 5 6 7 8
28 29 30 31 32 33 34 9
27 48 49 50 51 52 35 10
26 47 60 61 62 53 36 11
25 46 59 64 63 54 37 12
24 45 58 57 56 55 38 13
23 44 43 42 41 40 39 14
22 21 20 19 18 17 16 15
 */
function spiral(n){
    if(n === 1)
        return 1;
    
    let grid = [];
    for(let i = 0; i < n; i++)
        grid.push([]);
    
    let i = 0, j = 0, count = 1;
    let end = n*n;
    let dir = 0; //0=right,1=down,2=left,3=up.
    grid[i][j] = count;
    count++;
    while(count <= end){
        if(dir%4 === 0){  //right
            if(isValid(grid, n, i,  j+1)){
                j++;
                grid[i][j] = count;
                count++;       
            }
            else                        
                dir++;
        }if(dir%4 === 1){  //down
            if(isValid(grid, n, i+1,j)){
                i++;
                grid[i][j] = count;
                count++;
            }
            else                        
                dir++;
        }if(dir%4 === 2){  //left
            if(isValid(grid, n, i,  j-1)){
                j--;
                grid[i][j] = count;
                count++;
            }
            else                       
             dir++;
        }if(dir%4 === 3){  //up
            if(isValid(grid, n, i-1,j)) {
                i--;
                grid[i][j] = count;
                count++;
            }
            else                       
             dir++;
        }
    }
    return grid;
}
function isValid(grid, n, i,j){
    //checks inbounds and not already taken
    return i>=0 && j>=0 && i < n &&  j< n && grid[i][j] == undefined;
}
console.log(spiral(4));





const spiral2 = number => {
    let counter = 1;
    let startRow = 0,
        endRow = number - 1;
    let startColumn = 0,
        endColumn = number - 1;

    const matrix = [];
    for (let i = 0; i < number; i++) 
        matrix.push([]);

    while (startColumn <= endColumn && startRow <= endRow) {
        // Start Row
        for (let i = startColumn; i <= endColumn; i++) {
            matrix[startRow][i] = counter;
            counter++;
        }
        startRow++;

        // End Column
        for (let i = startRow; i <= endRow; i++) {
            matrix[i][endColumn] = counter;
            counter++;
        }
        endColumn--;

        // End Row
        for (let i = endColumn; i >= startColumn; i--) {
            matrix[endRow][i] = counter;
            counter++;
        }
        endRow--;

        // Start Column
        for (let i = endRow; i >= startRow; i--) {
            matrix[i][startColumn] = counter;
            counter++;
        }
        startColumn++;
    }

    return matrix;
};
console.log(spiral2(3));