/*
http://blog.gainlo.co/index.php/2016/07/12/meeting-room-scheduling-problem/

meeting times: (1, 4), (5, 6), (8, 9), (2, 6).
return minimum number of meeting rooms

*/

arr = [[1,4],[5,6],[8,9],[2,6]];
arr.sort();
// [ [ 1, 4 ], [ 2, 6 ], [ 5, 6 ], [ 8, 9 ] ]

maxRooms = 0;
rooms = 0;
startIndex = 0;
endIndex = 0;

while(endIndex < arr.length-1){
    //console.log(startIndex,endIndex,rooms);
    if(arr[startIndex][0] < arr[endIndex][1]){
        rooms++;
        if(rooms > maxRooms)
            maxRooms = rooms;
        if(startIndex < arr.length-1)
            startIndex++;
    }
    else if(arr[startIndex][0] > arr[endIndex][1]){
        rooms--;
        endIndex++;
    }
}
//console.log('you need ', maxRooms, ' rooms.');

/*
Similar problem - 
Given a list of people with their birth and death years, find the year with the highest population.
https://vimeo.com/158532188 pwd: FB_IPS

say i have arr = [[1,4],[5,6],[8,9],[2,6]];
can view it as
start  1,5,8,2
end    4,6,9,6
I quicksort nlogn
start  1,2,5,8
end    4,6,6,8
then on every start < end, count++ and start++
    check on every count++, if its the max.
on every start > end, count-- and end++
O(n + n)
=total time O(nlogn)
*/

let highestPop = function(array) {
    let start = [], end = [];
    for(let i = 0; i < array.length; i++){ //O(2n)
        start.push(array[i][0]);
        end.push(array[i][1]);
    }
    start.sort(); //quicksort O(nlogn)
    end.sort();

    let maxCount = 0, count = 0;
    let endIndex = 0, startIndex = 0;
    let maxYear = false;
    while(endIndex < array.length){
        console.log(start, end, startIndex, endIndex);

        if(start[startIndex] && start[startIndex] < end[endIndex]){
            startIndex++;
            count++;            
            if(count > maxCount){
                maxCount = count;
                maxYear = start[startIndex];
            }
        }
        else{ //if(start[startIndex] > end[endIndex]){
            endIndex++;
            count--;                        
        }
    }
    return maxYear;
}
console.log( highestPop(arr) );
