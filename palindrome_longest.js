var longestPalindrome = function(str) {
  var length = str.length;
  var result = "";

  var centeredPalindrome = function(left, right) {
    while (left >= 0 && right < length && str[left] === str[right]) {
      //expand in each direction.
      left--;
      right++;
    }
    return str.slice(left + 1, right);
  };

  for (var i = 0; i < length - 1; i++) {
    var evenPal = centeredPalindrome(i, i + 1);
    var oddPal = centeredPalindrome(i, i);

    if (oddPal.length > 1) console.log("oddPal: " + oddPal);
    if (evenPal.length > 1) console.log("evenPal: " + evenPal);

    if (oddPal.length > result.length) result = oddPal;
    if (evenPal.length > result.length) result = evenPal;
  }
  return (
    "the palindrome is: " + result + " and its length is: " + result.length
  );
};

console.log(longestPalindrome("nan noon is redder"));
