/*
http://codercareer.blogspot.com/2013/02/no-44-maximal-stolen-values.html
There are n houses built in a line, each of which contains some value in it. A thief is going to steal the maximal value in these houses, but he cannot steal in two adjacent houses because the owner of a stolen house will tell his two neighbors on the left and right side. What is the maximal stolen value?

For example, if there are four houses with values 
{6, 1, 2, 7}, the maximal stolen value is 13 when the first and fourth houses are stolen.
 0 ,1 ,2, 3

brute force
1010..1010
1001..010101
0101..

take 1st 2 numbers[0,1], 
    if local max index == 0
        max+= arr[0]
        next 3 numbers start at index=2
    if local max index == 1
        max+=arr[1]
        next 3 numbers start at index=3
*/


function maxToSteal(arr){
    let index = 0;
    let max = 0;
    while(index < arr.length){
        if (arr[index] > arr[index+1]){
            max += arr[index];
            index += 2;
        }else{
            if(index == arr.length-1)
                max += arr[index];//end case          
            else
                max += arr[index+1];
            index += 3;            
        }        
    }
    return max;
}

console.log(maxToSteal([6,1,2,7]));
console.log('a', maxToSteal([3,2,3,2]));
console.log('b', maxToSteal([1,2,3,4]));
console.log('b', maxToSteal([4,3,2,1]));


//solution 2
function maxRec(arr){  //add memo?
    if(arr.length == 2)
        return Math.max(arr[0],arr[1]);
    if(arr.length == 1)
        return arr[0];        
    let arr1 = arr.slice(0,arr.length-1);
    let arr2 = arr.slice(0,arr.length-2);
    return Math.max(arr[arr.length-1] + maxRec(arr2), maxRec(arr1));
}


console.log(maxRec([6,1,2,7]));
console.log(maxRec([1,2,3,4]));
console.log(maxRec([4,3,2,1]));