/*
merge 2 lists

2 optimized solutions.
1. use a heap to pick from smallest.
2. merge 2 lists at a time.
*/

//1.
var sorted = [];
var pq = new MinPriorityQueue(function(a, b) {
  return a.number < b.number;
});

var indices = new Array(k).fill(0);

for (var i=0; i<k; ++i) if (sequences[i].length > 0) {
  pq.insert({number: sequences[i][0], sequence: i});
}

while (!pq.empty()) {
  var min = pq.findAndDeleteMin();
  sorted.push(min.number);
  ++indices[min.sequence];
  if (indices[min.sequence] < sequences[i].length) pq.insert({
    number: sequences[i][indices[min.sequence]],
    sequence: min.sequence
  });
}



//2. 

/**
 * Definition for singly-linked list.
 * function ListNode(val) {
 *     this.val = val;
 *     this.next = null;
 * }
 */
/**
 * @param {ListNode[]} lists
 * @return {ListNode}
 */
var mergeKLists = function(lists) {
    function mergeTwoLists(l1, l2) {
        var i = l1, j = l2;
        var res = new ListNode(-1);
        curr = res;
        while(i && j && i.val !== undefined && j.val !== undefined){
            if(i.val < j.val){
                curr.next = i;
                i = i.next;
            }else{
                curr.next = j;
                j = j.next;
            }
            curr = curr.next;     
        }
        if(i && i.val !== undefined){
            curr.next = i;
        }
        if(j && j.val !== undefined){
            curr.next = j;
        }
        return res.next || [];
    }
    
    var k;
    while(lists.length >= 2){
        var tmpList = [];
        for(k = lists.length - 1; k >= 0; k-=2){
            tmpList.push(mergeTwoLists(lists[k], lists[k - 1]));
        }
        if(k === 0){
            tmpList.push(lists[0]);
        }
        lists = tmpList;
    }
    
    return lists[0] || [];
};