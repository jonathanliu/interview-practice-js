/*
http://codercareer.blogspot.com/2011/10/no-10-k-th-node-from-end.html
Problem: Get the Kth node from end of a linked list. It counts 
from 1 here, so the 1st node from end is the tail of list. 

For instance, given a linked list with 6 nodes, whose value 
are 1, 2, 3, 4, 5, 6, its 3rd node from end is the node with value 4.

Ans: 
as you incrment, once you past k nodes, start the next pointer.
*/

function findk(head, k){
    let node = head;
    let knode = head;
    let c = 0;
    let kcount = 0;
    while(node.next){
        node = node.next;
        c++;
        if(c > k){
            knode = knode.next;
        }
    }
    return knode.next.data;
}
