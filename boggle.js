/* Boggle game - given a board of letters (2d array) and a word (string), 
return whether the word exists in the board. From each letter you can move in 
all directions (including diagonals), but you cannot use the same letter twice.  
https://www.glassdoor.com/Interview/Boggle-game-given-a-board-of-letters-2d-array-and-a-word-string-return-whether-the-word-exists-in-the-board-From-ea-QTN_1314647.htm
*/

let grid = [['A', 'B', 'O', 'P', 'L'],
            ['X', 'Y', 'P', 'X', 'R'],
            ['Q', 'S', 'T', 'A', 'L'],
            ['R', 'T', '5', 'M', 'L'],
            ['S', 'M', 'Y', 'P', 'Q']];

function find(word){
    let exists = false;
    //find 1st letter
    for(let i=0;i<grid.length;i++){
        for(let j=0;j<grid[0].length;j++){
            if(word[0] === grid[i][j]){
                
                if(word.length === 1)
                    return exists = true;
                let used = { [i]: { [j]: true}} ;

                used[i][j] = 1;
                //console.log('debug', word,used,i,j); 

                next( word.slice(1,word.length), used, i, j);

            }
        }   
    }
    
    function next(word, used, i ,j){ 
        //check neighbors and set boundaries
        let minI = (i===0)? 0 : i-1; 
        let maxI = (i===grid.length-1)? grid.length-1 : i+1;
        let minJ = (j===0)? 0 : j-1;
        let maxJ = (j===grid[0].length-1)? grid[0].length-1 : j+1;
        for(let ii = minI; ii <= maxI; ii++){   //check within boundaries
            for(let jj = minJ; jj <= maxJ; jj++){

                if(grid[ii][jj] == word[0]){   //check letter
                     if( !used.hasOwnProperty( {[ii]: jj} )){ //check if not used
                        if(used[ii])
                            used[ii][jj] = true;
                        else
                            used[ii] = {[jj]: true};

                        //console.log('debug', word, '\n',used,ii,jj);
                        if(word.length === 1)
                            return exists = true;
                        next(word.slice(1,word.length), used, ii ,jj); 
                    }
                }
            }   
        }
        return;
    }
    return exists;
}
console.log(find('MLQP'));



